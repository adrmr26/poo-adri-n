import java.util.Scanner;

public class Cliente extends Usuario{

	public Cliente(String nombreUsuario, String password) {
		super(nombreUsuario, password);

	}
	
	public void completarPerfilAlimentario() {
		
		Scanner  entrada = new Scanner(System.in);
		
		PerfilAlimentario perfilalimentario = null;
		
		System.out.println(" ----- Complete el Perfil Alimentario ----- \n ");
		System.out.println("1) Comida Vegana ");
		System.out.println("2) Comida Vegetariana ");
		System.out.println("3) Comida Libre de Gluten ");
		System.out.println("4) Comida Halal ");
		System.out.println("5) Comida Kosher");
		System.out.println("6) Alergia ");
		
		int opcion = 0;
		
		switch(opcion) {
		
		case 1:
			perfilalimentario.setVegano(true);
			break;
		
		case 2:
			perfilalimentario.setVegetariano(true);
			break;
			
		case 3:
			perfilalimentario.setLibreGluten(true);
			break;
			
		case 4:
			perfilalimentario.setHalal(true);
			break;
			
		case 5:
			perfilalimentario.setKosher(true);
			break;
			
		case 6:
			
			System.out.println("Alimento al que le da alergia");
			
			String alergia = entrada.next();
			perfilalimentario.setAlergia(alergia);
			
			break;
			
		
		}
		
	}
	
	public void hacerPedido() {
		
		
		Scanner  entrada = new Scanner(System.in);
		
		Pedido pedido = null;
		Plato plato = null;
		
		System.out.println(" ----- Pedido ----- \n ");
		
		System.out.println("1) Alimento ");
		System.out.println("2) Direccion ");
		System.out.println("3) Salir");
		
		int opcion = 0;
		
		while(opcion != 3) {
			
			switch(opcion) {
			
			case 1:
				
				plato.hacerpedido();
			
				
			case 2:
				pedido.hacerPedido();
			
			}
		}
	}
}
