import java.util.Scanner;

public class Plato {
	
	private String nombrePlato;
	private float precio;
	private Receta receta;
	
	
	// Constructor
	
	public Plato(String nombrePlato, float precio, Receta receta) {
		this.nombrePlato = nombrePlato;
		this.precio = precio;
		this.receta = receta;
	}


	public String getNombrePlato() {
		return nombrePlato;
	}


	public void setNombrePlato(String nombrePlato) {
		this.nombrePlato = nombrePlato;
	}


	public float getPrecio() {
		return precio;
	}


	public void setPrecio(float precio) {
		this.precio = precio;
	}


	public Receta getReceta() {
		return receta;
	}


	public void setReceta(Receta receta) {
		this.receta = receta;
	}
	
	public void hacerpedido() {
		
		Scanner  entrada = new Scanner(System.in);
		
		System.out.println("Digite nombre del platillo");
		String nombrePlato = entrada.next();
		this.nombrePlato = nombrePlato;
		
		
	}
	
	public void precioplatoIVA() {
		
		double precioIVA = receta.precioReceta() * 0.13;
		double precioGanancia = receta.precioReceta() * 0.35;
		double precioTotal = precioIVA + precioGanancia;
		
		
		System.out.println("El monto a pagar es de: " + precioTotal + " Colones");
		
		
		
	}

}
