import java.util.ArrayList;

public class Despensa {
	
	private ArrayList<Ingrediente> ingredientes;
	
	
	// Constructor

	public Despensa(ArrayList<Ingrediente> ingredientes) {
		super();
		this.ingredientes = ingredientes;
	}

	public ArrayList<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(ArrayList<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	
	// Metodos
	
	public void agregarIngrediente(Ingrediente ingrediente) {
		
		ingredientes.add(ingrediente);
		
	}
	
	public void eliminarIngrediente(Ingrediente ingrediente) {
		
		ingredientes.remove(ingrediente);
		
	}

}
