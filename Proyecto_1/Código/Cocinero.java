import java.util.Scanner;

public class Cocinero extends Usuario{
	
	// Constructor

	public Cocinero(String nombreUsuario, String password) {
		super(nombreUsuario, password);
		
	}
	
	
	public void agregarItemDespensa() {
		
		Scanner  entrada = new Scanner(System.in);
		
		Ingrediente  ingrediente = null;

		System.out.println("Digite el nombre del ingrediente: ");
		String nombreIgrediente = entrada.next();
		ingrediente.setNombreIngrediente(nombreIgrediente);
		
		System.out.println("Digite la cantidad: ");
		int cantidad = entrada.nextInt();
		ingrediente.setCantidad(cantidad);
		
		System.out.println("Digite el precio: ");
		float costo = entrada.nextFloat();
		ingrediente.setCosto(costo);
		
		Despensa despensa = null;
		
		despensa.agregarIngrediente(ingrediente);
		
		System.out.println("----- Agregaste a la despensa -----\n");
		System.out.println("Nombre: " + ingrediente.getNombreIngrediente());
		System.out.println("Cantidad: "+ ingrediente.getCantidad());
		
		
	}
	
	public void eliminarItemDespensa() {
		
		Scanner  entrada = new Scanner(System.in);
		
		Despensa despensa = null;
		
		Ingrediente  ingrediente = null;
		
		System.out.println("Digite el nombre del ingrediente: ");
		
		String nombreIngrediente = entrada.next();
		
		if(nombreIngrediente == ingrediente.getNombreIngrediente()) {
			
			despensa.eliminarIngrediente(ingrediente);
			
			System.out.println("---- Sacaste de la despensa ----");
			System.out.println("Nombre: " + ingrediente.getNombreIngrediente());
			
		}
		
		else {
			
			System.out.println("---- El ingrediente no se encuentra en la despensa ----");
		}
		


	}
	
	public void agregarReceta() {
		
		Scanner entrada = new Scanner(System.in);
		
		Receta receta = null;
		
		System.out.println("Digite el nombre de la receta: ");
		String nombreReceta = entrada.next();
		receta.setNombreReceta(nombreReceta);
		
		System.out.println("Preparacion: ");
		String preparacion = entrada.next();
		receta.setPreparacion(preparacion);;
		
		
		System.out.println("Digite la cantidad de ingredientes: ");
		
		int cantidad = 0;
		Ingrediente ingrediente = null;
		
		for(int i = 0;i<cantidad;i++) {
			
			System.out.println("Agregar ingrediente: ");
			String nombreIngrediente = null;
			ingrediente.setNombreIngrediente(nombreIngrediente);
			
			System.out.println("Agregar la cantidad ");
			int peso = 0;
			ingrediente.setCosto(peso);
			
			
			System.out.println("Agregar el precio: ");
			int costo = 0;
			ingrediente.setCosto(costo);
			
			receta.agregarIngrediente(ingrediente);
			
		}
		
		
	}
	

}
