import java.util.Scanner;

public class Administrador extends Usuario {
	
	// Constructor

	public Administrador(String nombreUsuario, String password) {
		super(nombreUsuario, password);
	
	}
	
	
	// Metodos
	
	public void agregarItemDespensa() {
		
		Scanner  entrada = new Scanner(System.in);
		
		Ingrediente  ingrediente = null;

		System.out.println("Digite el nombre del ingrediente: ");
		String nombreIgrediente = entrada.next();
		ingrediente.setNombreIngrediente(nombreIgrediente);
		
		System.out.println("Digite la cantidad: ");
		int cantidad = entrada.nextInt();
		ingrediente.setCantidad(cantidad);
		
		System.out.println("Digite el precio: ");
		float costo = entrada.nextFloat();
		ingrediente.setCosto(costo);
		
		Despensa despensa = null;
		
		despensa.agregarIngrediente(ingrediente);
		
		System.out.println("----- Agregaste a la despensa -----\n");
		System.out.println("Nombre: " + ingrediente.getNombreIngrediente());
		System.out.println("Cantidad: "+ ingrediente.getCantidad());
		
		
	}
	
	public void eliminarItemDespensa() {
		
		Ingrediente ingrediente = null;
		
		Scanner  entrada = new Scanner(System.in);
		
		Despensa despensa = null;
		
		System.out.println("Digite el nombre del ingrediente: ");
		
		String nombreIngrediente = entrada.next();
		
		if(nombreIngrediente == ingrediente.getNombreIngrediente()) {
			
			despensa.eliminarIngrediente(ingrediente);
			
			System.out.println("---- Sacaste de la despensa ----");
			System.out.println("Nombre: " + ingrediente.getNombreIngrediente());
			
		}
		
		else {
			
			System.out.println("---- El ingrediente no se encuentra en la despensa ----");
		}
		


	}
	

}
