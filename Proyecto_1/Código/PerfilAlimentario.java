
public class PerfilAlimentario {
	
	
	private String alergia;
	
	private boolean vegano;
	private boolean vegetariano;
	private boolean libreGluten;
	private boolean halal;
	private boolean kosher;
	
	// Constructor
	
	
	public PerfilAlimentario(String alergia, boolean vegano, boolean vegetariano, boolean libreGluten, boolean halal,
			boolean kosher) {
		super();
		this.alergia = alergia;
		this.vegano = vegano;
		this.vegetariano = vegetariano;
		this.libreGluten = libreGluten;
		this.halal = halal;
		this.kosher = kosher;
	}
	
	// Getters

	public String getAlergia() {
		return alergia;
	}

	public boolean isVegano() {
		return vegano;
	}


	public boolean isVegetariano() {
		return vegetariano;
	}

	
	public boolean isLibreGluten() {
		return libreGluten;
	}

	
	public boolean isHalal() {
		return halal;
	}

	

	public boolean isKosher() {
		return kosher;
	}
	
	// Setters
	
	public void setAlergia(String alergia) {
		this.alergia = alergia;
	}
	  
	  
	public void setVegano(boolean vegano) {
		this.vegano = vegano;
	}

	public void setVegetariano(boolean vegetariano) {
		this.vegetariano = vegetariano;
	}
	
	public void setLibreGluten(boolean libreGluten) {
		this.libreGluten = libreGluten;
	}

	public void setHalal(boolean halal) {
		this.halal = halal;
	}
	
	public void setKosher(boolean kosher) {
		this.kosher = kosher;
	}
	
}