import java.util.ArrayList;

public class Receta {
	
	private String nombreReceta;
	private String preparacion;
	private ArrayList <Ingrediente> ingredientes;
	private Ingrediente ingrediente;
	
	
	
	// Constructor
	
	public Receta(String nombreReceta, String preparacion, ArrayList<Ingrediente> ingredientes) {
		super();
		this.nombreReceta = nombreReceta;
		this.preparacion = preparacion;
		this.ingredientes = ingredientes;
	}
	
	// Getters



	public String getNombreReceta() {
		return nombreReceta;
	}
	
	public String getPreparacion() {
		return preparacion;
	}
	
	public ArrayList<Ingrediente> getIngredientes() {
		return ingredientes;
	}


	
	// Setters

	public void setNombreReceta(String nombreReceta) {
		this.nombreReceta = nombreReceta;
	}


	public void setPreparacion(String preparacion) {
		this.preparacion = preparacion;
	}


	public void setIngredientes(ArrayList<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	// Metodos
	
	public void agregarIngrediente(Ingrediente ingrediente) {
		
		ingredientes.add(ingrediente);
		
	
	}

	public void eliminarIngrediente(Ingrediente ingrediente) {
	
		ingredientes.remove(ingrediente);
}

	public float precioReceta() {
		
		ingrediente = null;
		
		int cantidadIngredientes = ingredientes.size();
		
		float precioReceta = 0;
		
		for(int i = 0; i< cantidadIngredientes; i++) {
			
			precioReceta += ingrediente.getCosto();
			
		}
		
		return precioReceta;
	}
	
	

}
