import java.util.Scanner;

public class Pedido {
	

	private int precioTotal;
	private String ubicacion;
	private float latitud;
	private float longtud;
	private Plato plato;
	
	
	public Pedido(int precioTotal, String ubicacion, float latitud, float longtud, Plato plato) {

		this.precioTotal = precioTotal;
		this.ubicacion = ubicacion;
		this.latitud = latitud;
		this.longtud = longtud;
		this.plato = plato;
	}


	public int getPrecioTotal() {
		return precioTotal;
	}


	public void setPrecioTotal(int precioTotal) {
		this.precioTotal = precioTotal;
	}


	public String getUbicacion() {
		return ubicacion;
	}


	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}


	public float getLatitud() {
		return latitud;
	}


	public void setLatitud(float latitud) {
		this.latitud = latitud;
	}


	public float getLongtud() {
		return longtud;
	}


	public void setLongtud(float longtud) {
		this.longtud = longtud;
	}


	public Plato getPlato() {
		return plato;
	}


	public void setPlato(Plato plato) {
		this.plato = plato;
	}
	
	public void hacerPedido() {
		
		Plato plato = null;
		
		Scanner  entrada = new Scanner(System.in);
		
		System.out.println(" ----- Menu de la Direccion -----");
		System.out.println(" 1) Digite nombre de la calle");
		System.out.println(" 2) Digite la Latitud");
		System.out.println(" 3) Digite la Longitud");
		System.out.println(" 4) Salir");
		
		int opcion = 0;
		
		while (opcion != 4) {
			
			
			switch(opcion) {
			
			case 1:
				
				System.out.println(" Nombre de la calle: ");
				String calle  = entrada.next();
				this.ubicacion = calle;
				break;
				
			case 2:
				
				System.out.println(" Latitud: ");
				float lat = entrada.nextFloat();
				this.latitud = lat;
				break;
				
			case 3:
				System.out.println("Longitud: ");
				float lon = entrada.nextFloat();
				this.longtud = lon;
				break;
			
			}
			
		}
		
		
		
		
	}
	
	

}
