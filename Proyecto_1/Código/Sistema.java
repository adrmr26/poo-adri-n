
import java.util.ArrayList;
import java.util.Scanner;

public class Sistema {
	

	private ArrayList<Usuario> cuentaAdmi;
	private ArrayList<Usuario> cuentaCliente;
	private ArrayList<Usuario> cuentaCocinero;
		
	private Usuario cuentaActual;
		
	Scanner entrada = new Scanner(System.in);
	
	public Sistema() {
		
		this.cuentaAdmi = new ArrayList<>();
		this.cuentaCliente = new ArrayList<>();
		this.cuentaCocinero = new ArrayList<>();
		
	}
	
		
	public void agregarUsuarioAdmi (String nombreUsuario, String password) {
		
		cuentaAdmi.add(new Usuario(nombreUsuario, password,"admi"));
		
	}
	
	public void agregarUsuarioCliente (String nombreUsuario, String password) {
		
		cuentaCliente.add(new Usuario(nombreUsuario, password,"cliente"));
		
	}
	
	public void agregarCocinero (String nombreUsuario, String password) {
		
		cuentaCocinero.add(new Usuario(nombreUsuario, password, "cocinero"));
		
	}
	
	public int buscarCuenta(String nombreUsuario, String password,String tipoCuenta) {
		
		if(tipoCuenta == "admi") {
			for (int i = 0; i<cuentaAdmi.size(); i++){
				if((cuentaAdmi.get(i).getNombreUsuario() == nombreUsuario)&&(cuentaAdmi.get(i).getPassword() == password)) {
					return i;
				}
			}
			
		}
		
		else if(tipoCuenta == "cliente") {
			for(int i = 0; i<cuentaCliente.size(); i++) {
				if((cuentaCliente.get(i).getNombreUsuario() == nombreUsuario)&&(cuentaCliente.get(i).getPassword() == password)) {
					return i;
				}
				
			}
		}
		else {
			for(int i = 0; i < cuentaCocinero.size();i++) {
				if((cuentaCocinero.get(i).getNombreUsuario() == nombreUsuario)&&(cuentaCocinero.get(i).getPassword() == password)) {
					return i;
				}
				
			}
			
		}
		return 100;
	}
	
	public Usuario getUsuario(int posicion, String tipoCuenta) {
		
		if(tipoCuenta == "admi") {
			return cuentaAdmi.get(posicion);
		}
		else if(tipoCuenta == "cliente") {
			return cuentaCliente.get(posicion);
		}
		else {
			return cuentaCocinero.get(posicion);
		}
	}
		
	public void iniciarSesion(String nombreUsuario, String password) {
		
		String contrasena = null;
		String usuario = null;
		
		if(buscarCuenta(nombreUsuario, password, "admi") < 100) {
			
			this.cuentaActual = getUsuario(buscarCuenta(nombreUsuario,password,"admi"),"admi");
			System.out.print("Ingrese su nombre de Usuario: ");
			usuario = entrada.next();
			System.out.print("Ingrese su contrase�a: ");
			contrasena = entrada.next();
			
			while(!this.cuentaActual.verificarContrasena(contrasena)) {
				
				System.out.println("\nContrase�a Incorrecta. Por favor ingrese nuevamente su contrase�a");
	            contrasena = entrada.next();
				
			}
			
			System.out.println("\nIngres� al sistema exitosamente\n");
			System.out.println("\nBienvenido" + cuentaActual.getNombreUsuario());
			
			Administrador adm = null;
			
			System.out.println("\nIngres� al sistema exitosamente\n");
			System.out.println("\nBienvenido" + cuentaActual.getNombreUsuario());
			
			int choice = 0;
			
			System.out.println("------ Menu principal ----- \n");
			System.out.println("1) Agregar ingrediente a la despensa\n");
			System.out.println("2) Sacar ingrediente de la despensa\n");
			System.out.println("3) Salir\n");
			System.out.println("Ingrese se eleccion: ");
			
			
			
			choice = entrada.nextInt();
			
			while(choice != 3) {
				
				switch(choice) {
				
				case 1:
					
					adm.agregarItemDespensa();
					break;
					
				case 2:
					
					adm.eliminarItemDespensa();
					break;
				}
				
			}
			
		}
		
		else if(buscarCuenta(nombreUsuario, password, "cliente") < 100) {
			
			this.cuentaActual = getUsuario(buscarCuenta(nombreUsuario,password,"cliente"),"cliente");
			System.out.print("Ingrese su nombre de Usuario: ");
			usuario = entrada.next();
			System.out.print("Ingrese su contrase�a: ");
			contrasena = entrada.next();
			
			while(!this.cuentaActual.verificarContrasena(contrasena)) {
				
				System.out.println("\nContrase�a Incorrecta. Por favor ingrese nuevamente su contrase�a");
	            contrasena = entrada.next();
				
			}
			
			Cliente cliente = null;
			
			System.out.println("\nIngres� al sistema exitosamente\n");
			
			System.out.println("\nBienvenido" + cuentaActual.getNombreUsuario());
			
			int choice = 0;
			
			System.out.println("------ Menu Cliente ----- \n");
			System.out.println("1) Completar perfil alimentario\n");
			System.out.println("2) Hacer pedido\n");
			System.out.println("3) Salir\n");
			
			choice = entrada.nextInt();
			
			while(choice != 3) {
				
				switch(choice) {
				
				
				case 1:
					
					cliente.completarPerfilAlimentario();
					break;
					
				case 2:
					cliente.hacerPedido();
				
				}
	
			}

		}
		
		else if(buscarCuenta(nombreUsuario,password,"cocinero")<100){
			this.cuentaActual = getUsuario(buscarCuenta(nombreUsuario,password,"cocinero"),"cocinero");
			System.out.print("Ingrese su nombre de Usuario: ");
			usuario = entrada.next();
			System.out.print("Ingrese su contrase�a: ");
			contrasena = entrada.next();
			
			while(!this.cuentaActual.verificarContrasena(contrasena)) {
				
				System.out.println("\nContrase�a Incorrecta. Por favor ingrese nuevamente su contrase�a");
	            contrasena = entrada.next();
				
			}
			Cocinero chef = null;
			
			System.out.println("\nIngres� al sistema exitosamente\n");
			
			System.out.println("\nBienvenido" + cuentaActual.getNombreUsuario());
			
			int choice = 0;
			
			System.out.println("------ Menu Cocinero ----- \n");
			System.out.println("1) Agregar platillo\n");
			System.out.println("2) Agregar ingrediente a la despensa\n");
			System.out.println("3) Sacar ingrediente de la despensa\n");
			System.out.println("4) Salir\n");
			System.out.println("Ingrese se eleccion: ");
			
			
			
			choice = entrada.nextInt();
			
			while(choice != 4) {
				switch (choice) {
				
				case 1:
					
					chef.agregarReceta();
					break;
				
				case 2:
					
					chef.agregarItemDespensa();
					break;
					
				case 3:
					
					chef.eliminarItemDespensa();
					break;
				
				}
				
			}
			
			
		}
		
		else {
			
			System.out.println("Ingres� un nombre de usuario y contrasena de forma incorrecta");
			System.out.print("Ingrese nuevamente su nombre de usuario: ");
			usuario = entrada.next();
			System.out.print("Ingrese nuevamente su contrasena ");
			contrasena = entrada.next();
			
		}
			
				
	}
	
	
}
