
public class Ingrediente {
	
	private String nombreIngrediente;
	private int cantidad;
	private float costo;
	
	
	public Ingrediente(String nombreIngrediente, int cantidad, float costo) {
		super();
		this.nombreIngrediente = nombreIngrediente;
		this.cantidad = cantidad;
		this.costo = costo;
	}

	
	// Getters

	public String getNombreIngrediente() {
		return nombreIngrediente;
	}


	public int getCantidad() {
		return cantidad;
	}


	public float getCosto() {
		return costo;
	}

	
	// Setters
	
	
	public void setCosto(float costo) {
		this.costo = costo;
	}
	
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public void setNombreIngrediente(String nombreIngrediente) {
		this.nombreIngrediente = nombreIngrediente;
	}
	

}
