package application;


import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button Boton;

    @FXML
    private TextField ingresarNombre;

    @FXML
    private Label Nombre;

    @FXML
    void imprimir(ActionEvent event) {
    	
    	String nombre = ingresarNombre.getText();
    	Nombre.setText(nombre);


    }

    @FXML
    void initialize() {
        
    	
    }
}