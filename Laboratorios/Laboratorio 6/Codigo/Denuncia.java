
package laboratorio6;

import java.util.Date;

public class Denuncia {
    
    private String descripcion;
    private Date fecha;
    private int hora;
    
    // Constructor

    public Denuncia(String descripcion, Date fecha, int hora) {
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.hora = hora;
    }

  
    // Getters

    public String getDescripcion() {
        return descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public int getHora() {
        return hora;
    }
    

    // Setters

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }
    
 
    public void imprimirDenuncia(){
        
        System.out.println(fecha);
        System.out.println(hora);
        System.out.println("Descripcion de los hechos \n " + descripcion);
    
    
    }
    
}




