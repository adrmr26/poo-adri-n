/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio6;

import java.util.Date;

public class DenunciaEspecial extends Denuncia{
    
    private float latitud;
    private float longitud;
    
    
    // Constructor

    public DenunciaEspecial(float latitud, float longitud, String descripcion, Date fecha, int hora) {
        super(descripcion, fecha, hora);
        this.latitud = latitud;
        this.longitud = longitud;
    }
    
    // Getters

    public float getLatitud() {
        return latitud;
    }

    public float getLongitud() {
        return longitud;
    }
    
    // Setters

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }
    
    public void imprimirDenunciaEspecial (){
        
        System.out.println("Descripcion de los hechos \n " + getDescripcion());
        System.out.println("Fecha\n " + getFecha());
        System.out.println("Hora\n " + getHora());
        System.out.println("Latitud\n " + latitud);
        System.out.println("Longitud\n " + longitud);

    }
    
}
