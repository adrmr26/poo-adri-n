
package laboratorio6;

import java.util.ArrayList;

public class OIJ extends Usuario {
    
    
    private  Usuario propietario;
    private int carnet;
    private String puesto;
    
    //Areglo
    
    ArrayList<Denuncia> Denuncia = new ArrayList();
    ArrayList<DenunciaEspecial> DenunciaEspecial = new ArrayList();
    
    
    // Constructor

    public OIJ(int carnet, String puesto, String nombre, int cedula, int id, String password,String tipoCuenta) {
        
        super(nombre, cedula, id, password,tipoCuenta);
        this.carnet = carnet;
        this.puesto = puesto;
    }
    
    public ArrayList<Denuncia> getDenuncia(){
        
        return this.Denuncia;
    
    }
    
    public ArrayList<DenunciaEspecial> getDenunciaEspecial(){
        
        return this.DenunciaEspecial;
    
    }
  
   
    //Getters

    public int getCarnet() {
        return carnet;
    }

    public String getPuesto() {
        return puesto;
    }

    public Usuario getPropieratio() {
        return propietario;
    }
    
    
    

    // Setters

    public void setCarnet(int carnet) {
        this.carnet = carnet;
    }
 
    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }
    
    
    // Metodos
    
    public void verDenuncias(){
        
        if(Denuncia.isEmpty()){
            
            System.out.println("No hay denuncias");
        
        }
        
        for(int i = 0; i<Denuncia.size(); i++){
            
            System.out.println("------------------------------------------------------------------------\nDenuncia: \n "+(i+1));
            System.out.println(Denuncia.get(i).getDescripcion());
            System.out.println("Fecha: " + Denuncia.get(i).getFecha());
            System.out.println("Hora: " + Denuncia.get(i).getHora());

        }
        
        System.out.println("------------------------------------------------------------------------\n");
        
    }
    
    public void verDenunciaEspecial(){
        
        if(DenunciaEspecial.isEmpty()){
            
            System.out.println("No hay denuncia especiales");
       
        }
        
        for(int i = 0; i < DenunciaEspecial.size(); i++){
            
            System.out.println("------------------------------------------------------------------------\nDenuncia: \n" + (i+1));
            System.out.println(DenunciaEspecial.get(i).getDescripcion());
            System.out.println("Fecha: " + DenunciaEspecial.get(i).getFecha());
            System.out.println("Hora: " + DenunciaEspecial.get(i).getHora());
            System.out.println("Latitud: " + DenunciaEspecial.get(i).getLatitud());
            System.out.println("Longitud: " + DenunciaEspecial.get(i).getLongitud());
        
        }
    
    }
    
    
    public void imprimirOIJ(){
        
        System.out.println("Nombre: "+ getNombre());
        System.out.println("Cedula: "+ getCedula());
        System.out.println("ID: " + getId());
        System.out.println("Carnet : " + carnet);
        System.out.println("Puesto: "+ puesto);
  
    }
    
}
