
package laboratorio6;


public class Persona extends Usuario{
    
    

    public Persona(String nombre, int cedula, int id, String password,String tipoCuenta) {
        super(nombre, cedula, id, password,tipoCuenta);
    }
    
    
    public void imprimirPersona (){
        
        System.out.println("Nombre: " + getNombre());
        System.out.println("Cedula: " + getCedula());

    }
    
}
