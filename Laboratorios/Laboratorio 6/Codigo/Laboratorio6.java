/*
    Tecnologico de Costa Rica
    Laboratorio 6
    Bryan Campos Castro 2019341558
    Adrian Molina Rojas 2019068081

*/

// Main

package laboratorio6;

import java.util.Scanner;


public class Laboratorio6 {
    
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        
        Registro registro = new Registro();
        
        int choice = 0;
        int ID;
        
        registro.agregarCuentaOIJ(2019341558, "Gerente", "Bryan", 208098763, 232, "casa123", "OIJ");
    
        
        
        do{
            
            System.out.println("Sistema de Denuncias OIJ");
            System.out.println("(1) Iniciar Sesion");
            System.out.println("(0) Salir ");
            System.out.println("Opcion");
            choice = entrada.nextInt();
            
            if(choice == 1){
                System.out.println("Ingrese su ID: ");
                ID = entrada.nextInt();
                registro.iniciarSesion(ID);
            
            
            }
        
        
        }while(choice != 0);
        

    }
    
}
