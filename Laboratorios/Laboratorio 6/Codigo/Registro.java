
package laboratorio6;

import java.util.ArrayList;
import java.util.Scanner;


public class Registro {
    
    // Atributo
    
    private Usuario cuentaActual;
    
    // Arreglos
    
    private ArrayList<Usuario> cuentaOIJ;
    private ArrayList<Usuario> cuentaPersona;
    
    // Constructor
    
    public Registro(){
        this.cuentaOIJ = new ArrayList<>();
        this.cuentaPersona = new ArrayList<>();

    }
    
    // Metodos
    
    public void agregarCuentaOIJ(int carnet, String puesto, String nombre, int cedula, int id, String password,String tipoCuenta){
        
        cuentaOIJ.add(new OIJ(carnet,puesto,nombre,cedula,id,password,"OIJ"));
    }
    
    public void agregarCuentaPersona(String nombre, int cedula, int id, String password,String tipoCuenta){
        
        cuentaPersona.add(new Persona(nombre,cedula,id,password,"Persona"));

    }
    
    public int buscarUsuario(int id, String tipoUsuario){
        
        if("OIJ".equals(tipoUsuario)){                                           // Si el tipo de cuenta es igual a OIJ
            
            for(int i = 0; i<cuentaOIJ.size(); i++){
                
                if(cuentaOIJ.get(i).getId() == id){
                    
                    return i;
                    
                }
            }
        } 
        
        else {
            
            for (int i = 0; i < cuentaPersona.size(); i++){
                
                if(cuentaPersona.get(i).getId() == id){
                    
                    return i;
                
                }
            
            }
        }
        
        return 100;                                                              
    }
    
    public Usuario getUsuario(int posicion, String tipoCuenta){
        
        if("OIJ".equals(tipoCuenta)){
            
            return cuentaOIJ.get(posicion);
  
        }
        
        else{
            
            return cuentaPersona.get(posicion);
            
        }

    }
    
    public void iniciarSesion(int ID){
        
        Scanner entrada = new Scanner (System.in);
        
        String password = "";
        
        if(buscarUsuario(ID,"OIJ") < 100){
            
                this.cuentaActual = getUsuario(buscarUsuario(ID,"OIJ"),"OIJ");
                System.out.println("Ingrese su contrasena: ");
                password = entrada.next(password);
            
                 
        
                while(!this.cuentaActual.verificarPassword(password)){
                System.out.println("Contrasena incorrecta");
                System.out.println("Ingrese nuevamente la contasena");
            
                }
        
                System.out.println("Ingreso al sistema\n");
        
                System.out.println("Hola \n" + this.cuentaActual.getNombre());
    }
        else if (buscarUsuario(ID,"Persona") < 100){
            
                 this.cuentaActual = getUsuario(buscarUsuario(ID,"Persona"),"Persona");
                 
                System.out.println("Ingrese su contrasena: ");
                password = entrada.next(password);
                
                while(!this.cuentaActual.verificarPassword(password)){
                System.out.println("Contrasena incorrecta");
                System.out.println("Ingrese nuevamente la contasena");
            
                }
        }
                
        else{
                        
                System.out.println("Ingreso su ID incorrecto");
                System.out.println("Ingrese nuevamente su ID");
                ID = entrada.nextInt();
                iniciarSesion(ID);
 
                }

        }
    

        
        }
        
        
        
        
            


    

