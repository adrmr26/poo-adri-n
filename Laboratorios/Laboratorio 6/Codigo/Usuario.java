
package laboratorio6;

import java.util.ArrayList;


public class Usuario {
    
    // Atributos
    
    private String nombre;
    private int cedula;
    private int id;
    private String password;
    private String tipoCuenta;
    private Usuario usuario;
    
    
    // Constructor

    public Usuario(String nombre, int cedula, int id, String password,String tipoCuenta) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.id = id;
        this.password = password;
        this.tipoCuenta = tipoCuenta;
    }

    
    // Getters 

    public String getNombre() {
        return nombre;
    }

    public int getCedula() {
        return cedula;
    }

    public int getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public Usuario getUsuario() {
        return usuario;
    }
    
    
    
    
    
    // Setters

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    
    
    
    // Metodos
    
    public boolean verificarPassword(String password){
        return this.password == password;
    
    }
    
 
    
    
    public void imprimirUsuario(){
        
        System.out.println("Nombre: " + nombre);
        System.out.println("Cedula: "  + cedula);
        System.out.println("ID: " + id);
        System.out.println("Tipo de Cuenta: " + tipoCuenta);
    
    }
    
    
    
    public Denuncia denuncia(Denuncia denuncia){
        
        return denuncia;
    
    
    }
    
}
