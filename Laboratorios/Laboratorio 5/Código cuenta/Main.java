//Adrián Molina Rojas
//2019068081
import java.time.LocalDate;

public class Main {

	public static void main(String[] args) {
		System.out.println("Bienvenid@");
		Cuenta miCuenta = new Cuenta();
		miCuenta.setId(112);
		miCuenta.setBalance(500000);
		miCuenta.setTasadeInteresAnual(0.045);
		//2.A
		System.out.print("ID: ");
		System.out.println(miCuenta.getId());
		
		System.out.print("Balance de: ");
		System.out.println(miCuenta.getBalance());
		
		System.out.print("Tasa de interes anual: ");
		System.out.println(miCuenta.getTasadeInteresAnual());
		
		//2.B
		System.out.print("Deposito realizado hay: ");
		System.out.println(miCuenta.depositarDinero(150000));
		
		//2.C
		System.out.print("Retiro realizado quedó: ");
		System.out.println(miCuenta.retirarDinero(200000));
		
		//2.D
		System.out.print("Balance de : ");
		System.out.println(miCuenta.getBalance());
		
		System.out.print("Tasa de interes Mensual: ");
		System.out.println(miCuenta.obtenerTasaDeInteresMensual(miCuenta.getTasadeInteresAnual()));
		
		System.out.print("Fecha de creación de la cuenta: ");
		LocalDate creaciónCuenta =LocalDate.parse("2020-05-26");
		System.out.println(creaciónCuenta);
		
		//Fin de la primera cuenta
		
		System.out.println("");
		
		//2.E
		System.out.println("Cuenta #2");
		Cuenta miCuenta2 = new Cuenta();
		miCuenta2.setBalance(100000);
		miCuenta2.setTasadeInteresAnual(0.060);
		LocalDate creaciónCuenta2 =LocalDate.parse("2020-05-27");
		
		//2.F
		System.out.print("Balance de: ");
		System.out.println(miCuenta2.getBalance());
		
		System.out.print("Tasa de interes Mensual: ");
		System.out.println(miCuenta2.obtenerTasaDeInteresMensual(miCuenta.getTasadeInteresAnual()));
		
		System.out.print("Fecha de creación de la cuenta: ");
		System.out.println(creaciónCuenta2);

// Crea el cajero 
ATM cajero = new ATM();
}
}