//Adrián Molina Rojas
//2019068081

//Importación de Bibliotecas
import java.time.LocalDate;

public class Cuenta {

	private int Id;
	private double Balance;
	private double TasadeInteresAnual;
	private LocalDate FechaDeCreación;
        private double TasaDeInteresMensual;
        private double InteresMensual;
	

	//Constructor vacio
	public Cuenta(){
	}

  //Constructor:
	public Cuenta(int Id,double Balance) 
	{
		this.Id = Id;
		this.Balance = Balance;
	}
	
	
	// Metodos Set y Get

  //Set y Get de Id
	public void setId(int Id){
		this.Id = Id;
	}
	public int getId() {
		return Id;
	}
	
  //Set y Get Balance
	public void setBalance(double Balance) {
		this.Balance = Balance;
	}
	public double getBalance(){
		return Balance;
	}

	// Set y Get Tasa de Interes  
	public void setTasadeInteresAnual(double TasadeInteresAnual) {
		this.TasadeInteresAnual = TasadeInteresAnual;
	}
	public double getTasadeInteresAnual(){
		return TasadeInteresAnual;
	}

	// Get de la fecha
	public LocalDate getFechaDeCreación() {
		return this.FechaDeCreación;
	}
	
// Metodo para obtener el interes anual 
	public double obtenerTasaDeInteresMensual(double TasadeInteresAnual) {
		this.TasaDeInteresMensual = TasadeInteresAnual / 12;
		return TasaDeInteresMensual;
	}

 //Metodo para calcular el balance con interes
	public double calcularInteresMensual(double Balance ,double TasaDeInteresMensual){
		this.InteresMensual = Balance * TasaDeInteresMensual;
		return InteresMensual;
	}

  //Metodo para depositar Dinero
  public double depositarDinero(double Depositar){
  this.Balance = Balance + Depositar;
  return Balance;
}
  //Metodo para Retirar Dinero
  public double retirarDinero(double Retiro){
    this.Balance = Balance - Retiro;
    return Balance;
}
}