//Adrián Molina Rojas
//2019068081
import java.util.Scanner;

public class ATM{

//Se crea un constructor
  public ATM(){
  
  Cuenta[] Clientes = new Cuenta[10];

  //Se crean las cuentas de los clientes
    Clientes[0] = new Cuenta(1,100000);
    Clientes[1] = new Cuenta(1,100000);
    Clientes[2] = new Cuenta(2,100000);
    Clientes[3] = new Cuenta(3,100000);
    Clientes[4] = new Cuenta(4,100000);
    Clientes[5] = new Cuenta(5,100000);
    Clientes[6] = new Cuenta(6,100000);
    Clientes[7] = new Cuenta(7,100000);
    Clientes[8] = new Cuenta(8,100000);
    Clientes[9] = new Cuenta(9,100000);

    Scanner entrada = new Scanner(System.in);
    //Variables

    int opcion;
    int numero;

    boolean salir = false;
    boolean bucle = true;

    double Retiro;
    double Depositar;

    while(bucle == true){
        
       System.out.println("Ingrese su Id:");
       numero = entrada.nextInt();

       if (numero >= 0 && numero < 10) {

         salir = false;

         while(salir == false){

           //Menu de interaccion con el usuario
            System.out.println("");
            System.out.println("Menu Principal");
            System.out.println("1. Ver el balance actual");
            System.out.println("2. Retirar Dinero");
            System.out.println("3. Depositar Dinero");
            System.out.println("4. Salir");
          
          //Pide la eleccion y la guarda
            System.out.println("Ingrese su selección.");
            opcion = entrada.nextInt();      
            
            switch(opcion){ 

              case 1:
               System.out.print("El balance es de: ");
               System.out.println(Clientes[numero].getBalance());
               break;
              case 2:
                System.out.println("Ingrese una cantidad para retirar");
                Retiro = entrada.nextDouble();
                Clientes[numero].retirarDinero(Retiro);
                break;
              case 3:
                System.out.println("Ingrese una cantidad para depositar");
                Depositar = entrada.nextDouble();
                Clientes[numero].depositarDinero(Depositar);
                break;
              case 4:

                System.out.println("Salio del programa");
                salir=true;
                break;

              default:
                System.out.println("Solo opciones entre 1 y 4");
            }
           }
       }
         else
         {
               System.out.println("Id invalido");
           }
      }
    
  }
}                  
