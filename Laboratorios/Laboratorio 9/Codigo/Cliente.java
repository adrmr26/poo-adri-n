import java.util.Random;

public class Cliente {
	
	
	private final int Min_tamano_carrito = 10;
	private final int Adicional_tamano_carrito = 5;
	private final int Max_codigo_producto = 100;

	
	private String nombre;
	private int[] carroCompra;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int[] getCarroCompra() {
		return carroCompra;
	}

	public Cliente(String nombre) {
		this.nombre = nombre;
		Random r = new Random();                                                                     
		int tamanoCarroCompra = Min_tamano_carrito + r.nextInt(Adicional_tamano_carrito + 1);        
		carroCompra = new int[tamanoCarroCompra];                                                    
		for(int i = 0; i< carroCompra.length;i++) {                                                 
			carroCompra[i] = r.nextInt(Max_codigo_producto + 1);                                     
			
			// System.out.println(carroCompra[i]);                                                   
			
		}
	}

}
