
public class Main {

	public static void main(String[] args) {
		/*
		Cliente cliente = new Cliente("Adrian");
		Cajera cajera = new Cajera("Marta");
		long inicialTime = System.currentTimeMillis();
		cajera.procesarCompra(cliente, inicialTime);
		Cliente cliente1 = new Cliente("Adres");
		inicialTime = System.currentTimeMillis();
		cajera.procesarCompra(cliente1, inicialTime);
		*/
		
		Cliente cliente = new Cliente ("Adrian");
		Cliente cliente2 = new Cliente ("Andres");
		long tiempoInicial = System.currentTimeMillis();
		
		CajeraThread cajera = new CajeraThread("Marta", cliente, tiempoInicial);
		CajeraThread cajero = new CajeraThread("Pedro", cliente2, tiempoInicial);
		
		cajera.start();
		cajero.start();
		
	}

}
