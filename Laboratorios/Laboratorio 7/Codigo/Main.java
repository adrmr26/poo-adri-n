import java.util.Scanner;

/*
 * 	Tecnologico de Costa Rica
 *  Laboratorio 7
 *  Adri�n Molina Rojas
 *  2019068081
 */
public class Main {

	public static void main(String[] args) {
		JSONParser parser = new JSONParser();
		Controlador controlador = parser.cargarAparatos();
		boolean bandera = true;
		
		while(bandera) {
			
			System.out.println("");
			System.out.println("  Electrodomesticos inteligentes \n");
			System.out.println(" 1) Ver Estado de los electrodomesticos \n");
			System.out.println(" 2) Enceder Electrodomestico \n");
			System.out.println(" 3) Apagar Electrodomestico \n");
			System.out.println(" 4) Salir \n");
			System.out.println("Opcion \n");
			Scanner entrada = new Scanner(System.in);
			
			int opcion;
			
			opcion = entrada.nextInt();
			
			switch(opcion) {
			
			case 1:
				
				System.out.println(controlador);
				
				break;
				
			case 2:
				
				int choiceOn;
				
				System.out.println("Digite el ID del electrodomestico que desea encender \n");
				
				choiceOn = entrada.nextInt();
				
				if(controlador.size() < choiceOn) {
					
					System.out.println("El ID no existe\n");
					
				}
				
				else {
					
					controlador.get(choiceOn).encender();
					
				}
				
				
				break;
				
			case 3:
				
				int choiceOff;
				
				System.out.println("Digite el ID del electrodomestico que desea apagar \n");
				
				choiceOff = entrada.nextInt();
				
				if(controlador.size() < choiceOff) {
					
					System.out.println("El ID no existe\n");
					
				}
				
				else {
					
					controlador.get(choiceOff).apagar();
					
				}
				
				break;
				
				
			case 4:
				
				bandera = false;
				break;
			
			}
			

			
		}
		


	}
	}