package ch.makery.address.model;

public class CasoCovid {
	
	private String Pais;
	private String Fecha;
	private int Acumulados;
	private int Activos;
	private int Muertes;
	private int Recuperados;
	
	private double TasaRecuperacion;
	private double TasaMortalidad;
	
	public CasoCovid(String pais, String fecha, int acumulados, int muertes, int recuperados) {
		Pais = pais;
		Fecha = fecha;
		Acumulados = acumulados;
		this.Activos = (acumulados-(muertes+recuperados));
		this.TasaRecuperacion = (recuperados%acumulados)/100 ;
		this.TasaMortalidad = (muertes%acumulados) /100;
		Muertes = muertes;
		Recuperados = recuperados;
	}	
	
	public String getPais() {
		return Pais;
	}
	
	public void setPais(String pais) {
		Pais = pais;
	}
	
	public String getFecha() {
		return Fecha;
	}
	
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
	
	public int getAcumulados() {
		return Acumulados;
	}
	
	public void setAcumulados(int acumulados) {
		Acumulados = acumulados;
	}
	
	public int getMuertes() {
		return Muertes;
	}
	
	public void setMuertes(int muertes) {
		Muertes = muertes;
	}
	
	public int getRecuperados() {
		return Recuperados;
	}
	
	public void setRecuperados(int recuperados) {
		Recuperados = recuperados;
	}

	public int getActivos() {
		return Activos;
	}

	public void setActivos(int activos) {
		Activos = activos;
	}

	public double getTasaMortalidad() {
		return TasaMortalidad;
	}

	public void setTasaMortalidad(double tasaMortalidad) {
		TasaMortalidad = tasaMortalidad;
	}

	public double getTasaRecuperacion() {
		return TasaRecuperacion;
	}

	public void setTasaRecuperacion(double tasaRecuperacion) {
		TasaRecuperacion = tasaRecuperacion;
	}


}
